using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Portal.jka.vn.Data;
using Portal.jka.vn.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
var configuration = new ConfigurationBuilder()
    .SetBasePath(builder.Environment.ContentRootPath)
    .AddJsonFile("appsettings.json")
    .Build();
string connectionString = configuration.GetConnectionString("DefaultConnection");
builder.Services.AddLogging(loggingBuilder =>
{
    loggingBuilder.AddFilter((category, level) =>
    {
        return level >= LogLevel.Warning;
    });
});
builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseNpgsql(connectionString));
builder.Services.AddTransient<IProjectServices, ProjectServices>();
builder.Services.AddTransient<SeedDataDbContext>();
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(30);
});

var app = builder.Build();
using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    var userDataService = services.GetRequiredService<SeedDataDbContext>();
    userDataService.Initialize();
}
app.UseSession();
// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
