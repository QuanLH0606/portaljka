﻿$(function () {
    var id = $(".projectId").val();
    $('#t2 thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#t2 thead');
    var table = $('#t2').DataTable({
        "scrollX": true,
        scrollY: '70vh',
        scrollCollapse: true,
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        orderCellsTop: true,
        fixedHeader: true,
        info: false,
        order: [[1, 'desc']],
        "ajax": {
            "url": "/Project/LoadDataProject",
            "type": "Get",
            "dataSrc": ""
        },
        "columns": [
            {
                "data": "id", render: function (data, type, row) {
                    return '<span id="pinput"><input type="radio"  name="Iditem" id="Ischecked" value="' + data + '"><label for="itemno"></label></span>'
                        + '<a data-toggle="modal" data-target="#FormDetails" type="button" onclick="DetailPaymentOrder(this)" class="view" title="Detail" ></a>';
                }, "class": "text-center text-nowrap"
            },
            {
                "data": null, render: function (data, type, row) {
                    return '<a style="color: #000;text-decoration:none;cursor:pointer;" href="/project/check/' + data.id + '">' + data.id + '</a>'
                }, "class": "text-nowrap"
            },
            {
                "data": null, render: function (data, type, row) {
                    return '<a style="color: #000;text-decoration:none;cursor:pointer;" href="/project/check/' + data.id + '" >' + data.reports + '</a>'
                }, "class": "text-nowrap "
            },
            { "data": "status", "class": "text-nowrap" },
            { "data": "totalRecord", "class": "text-nowrap" },
            { "data": "remainCheck", "class": "text-nowrap" },
            { "data": "remainSupervisor", "class": "text-nowrap" },
        ],
        initComplete: function () {
            var api = this.api();
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    $(cell).html('<input type="text" class="inputSearch" placeholder="' + title + '" />');
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('keyup change', function (e) {
                            e.stopPropagation();
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();

                            var cursorPosition = this.selectionStart;
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();

                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });

});