﻿$(function () {
    var id = $(".projectId").val();
    $('#t2 thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#t2 thead');
    var table = $('#t2').DataTable({
        "scrollX": true,
        scrollY: '70vh',
        scrollCollapse: true,
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        orderCellsTop: true,
        fixedHeader: true,
        info: false,
        order: [[1, 'desc']],
        "ajax": {
            "url": "/Project/ReportUserProject/?id=" + id,
            "type": "Get",
            "dataSrc": ""
        },
        "columns": [
            {
                "data": "username", render: function (data, type, row) {
                    return '<span id="pinput"><input type="radio"  name="Iditem" id="Ischecked" value="' + data + '"><label for="itemno"></label></span>'
                        + '<a data-toggle="modal" data-target="#FormDetails" type="button" onclick="DetailPaymentOrder(this)" class="view" title="Detail" ></a>';
                }, "class": "text-center text-nowrap"
            },
            { "data": "username", "class": "text-nowrap" },
            { "data": "completed", "class": "text-nowrap" },
            { "data": "found", "class": "text-nowrap" },
            { "data": "percent", "class": "text-nowrap" },
            { "data": "timeSpent", "class": "text-nowrap" },
            { "data": "recordHours", "class": "text-nowrap" },
            { "data": "qAd", "class": "text-nowrap" },
            { "data": "falsePos", "class": "text-nowrap" },
            { "data": "falseNeg", "class": "text-nowrap" },
            { "data": "percenQAd", "class": "text-nowrap" },
            { "data": "accuracy", "class": "text-nowrap" },
            { "data": "revisit", "class": "text-nowrap" },
        ],
        initComplete: function () {
            var api = this.api();
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    $(cell).html('<input type="text" class="inputSearch" placeholder="' + title + '" />');
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('keyup change', function (e) {
                            e.stopPropagation();
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();

                            var cursorPosition = this.selectionStart;
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();

                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });

});