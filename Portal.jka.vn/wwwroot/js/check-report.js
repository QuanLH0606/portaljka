﻿$(function () {
    var id = $(".projectID").val();
    var currentElementIndex = 0;
    var result;

    function displayDataAtIndex(index) {
        var data = result[index];
        $(".dataInforID").val(data.id);
        $(".merchanDBA").text(data.merchantDBA);
        $(".legalBusiness").text(data.legalBusiness);
        $(".billingDescriptor").text(data.billingDescriptor);
        $(".address1").text(data.address1);
        $(".suggestUrl").text(data.suggestedUrl);
        $(".address2").text(data.address2);
        $(".cityRegion").text(
            data.city + "/" + data.regionState + "/" + data.country + "/" + data.postalZipCode
        );
        $(".merchantDBACity").text(data.merchantDBA + " " + data.city);
        $(".legalBussinessCity").text(data.legalBusinessCity + " " + data.city);
        $(".phoneNumber").text(data.phoneNumber);
        $(".emailAddress").text(data.emailAddress);
        $(".contactName").text(data.contactName);
        var popupWindow = window.open(data.suggestedUrl, "popupWindowName", "width=800,height=1500");
        var address = data.address1 + "," + data.city + "," + data.postalZipCode + "," + data.regionState + "," + data.country;
        var encodedAddress = encodeURIComponent(address);
        var googleMapsUrl = "https://www.google.com/maps?q=" + encodedAddress;
        var searchMap = document.getElementById("search-map");
        searchMap.href = googleMapsUrl;
    }

    $.ajax({
        url: "/Project/LoadDataInfors",
        type: "GET",
        data: { projectID: id },
        success: function (response) {
            result = response.dataInfors;
            displayDataAtIndex(currentElementIndex);
            $(".totalRecord").text(response.totalDataInfors);
            $(".recordWorking").text(response.exportDatas);
            $(".completeCurrentProject").text(response.completeCurrentProject);
            $(".rateAllProject").text(response.ratePerHours);
            $(".completeAllProject").text(response.completeToday);
        },
        error: function (error) {
            console.log("Error:", error);
        },
    });

    $(".btnExport").on("click", function () {
        var dataId = $(".dataInforID").val();
        var urls = $(".suggestUrl").val();
        $.ajax({
            url: "/Project/ExportData",
            type: "Post",
            data: { projectID: id, dataInforID: dataId, url: urls },
            success: function (response) {
                $(".recordWorking").text(response.exportDatas);
                $(".completeCurrentProject").text(response.completeCurrentProject);
                $(".rateAllProject").text(response.ratePerHours);
                $(".completeAllProject").text(response.completeToday);
            }

        });
        currentElementIndex++;
        if (currentElementIndex < result.length) {
            displayDataAtIndex(currentElementIndex);
        }
    });
    $(".btnSupervisor").click(function () {
        currentElementIndex++;
        if (currentElementIndex < result.length) {
            displayDataAtIndex(currentElementIndex);
            var dataId = $(".dataInforID").val();
            $.ajax({
                url: "/Project/SuppervisorCheck",
                type: "Post",
                data: { projectID: id, dataInforID: dataId },
                success: function (response) {
                }
            });
        }
    });
    $("#revisits").change(function () {
        var check = $(this).prop("checked");
        if (check) {
            currentElementIndex++;
            if (currentElementIndex < result.length) {
                displayDataAtIndex(currentElementIndex);
                var dataId = $(".dataInforID").val();
                $.ajax({
                    url: "/Project/RevisitCheck",
                    type: "Post",
                    data: { projectID: id, dataInforID: dataId },
                    success: function (response) {
                        $("#revisits").prop("checked", false);
                    }
                });
            }

        }
    })
})