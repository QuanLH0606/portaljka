﻿$(".btnViewReport").click(function () {
    var id = $("input[name='Iditem']:checked").val();
    window.location.href = "/Project/ViewReport/?id=" + id;
});
$(function () {
    $(".btnViewQA").click(function () {
        var selectedValue = $("input[name='Iditem']:checked").val();
        window.location.href = "/Project/ViewQAReport/?id=" + selectedValue;
    });
    $(".btnViewSummary").click(function () {
        var selectedValue = $("input[name='Iditem']:checked").val();
        window.location.href = "/Project/ViewUserSummary/?id=" + selectedValue;
    });

    $('#t2 thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#t2 thead');
    var table = $('#t2').DataTable({
        "scrollX": true,
        scrollY: '70vh',
        scrollCollapse: true,
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        orderCellsTop: true,
        fixedHeader: true,
        info: false,
        order: [[1, 'desc']],
        dom: 'Bfrtip', // Hiển thị các nút export
        buttons: [
            'copy', 'csv', 'xlsx', 'pdf', 'print' // Các loại nút export
        ],
        "ajax": {
            "url": "/Project/LoadProjectQA",
            "type": "Get",
            "dataSrc": ""
        },
        "columns": [
            {
                "data": "id", render: function (data, type, row) {
                    return '<span id="pinput"><input type="radio"  name="Iditem" id="Ischecked" value="' + data + '"><label for="itemno"></label></span>'
                        + '<a data-toggle="modal" data-target="#FormDetails" type="button" onclick="DetailPaymentOrder(this)" class="view" title="Detail" ></a>';
                }, "class": "text-center text-nowrap"
            },
            { "data": "id", "class": "text-nowrap" },
            { "data": "project", "class": "text-nowrap" },
            { "data": "imported", "class": "text-nowrap" },
            { "data": "duplicate", "class": "text-nowrap" },
            { "data": "reviewed", "class": "text-nowrap" },
            { "data": "urls", "class": "text-nowrap" },
            { "data": "records", "class": "text-nowrap" },
            { "data": "revisit", "class": "text-nowrap" },
        ],
        initComplete: function () {
            var api = this.api();
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    $(cell).html('<input type="text" class="inputSearch" placeholder="' + title + '" />');
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('keyup change', function (e) {
                            e.stopPropagation();
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();

                            var cursorPosition = this.selectionStart;
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();

                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });

});