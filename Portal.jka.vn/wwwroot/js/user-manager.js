﻿const daySelects = document.querySelectorAll('.daySelect');
const monthSelects = document.querySelectorAll('.monthSelect');
const yearSelects = document.querySelectorAll('.yearSelect');

const currentDate = new Date();
const currentDay = currentDate.getDate();
const currentMonth = currentDate.getMonth();
const currentYear = currentDate.getFullYear();

function populateDays(select, month, year) {
    select.innerHTML = '';

    const daysInMonth = new Date(year, month + 1, 0).getDate();

    for (let day = 1; day <= daysInMonth; day++) {
        const option = document.createElement('option');
        option.value = day;
        option.textContent = day;
        select.appendChild(option);
    }
}

function populateMonths(select) {
    select.innerHTML = '';

    const months = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ];

    for (let month = 0; month < months.length; month++) {
        const option = document.createElement('option');
        option.value = month;
        option.textContent = months[month];
        select.appendChild(option);
    }
}

function populateYears(select) {
    const currentYear = new Date().getFullYear();

    for (let year = currentYear; year >= currentYear - 100; year--) {
        const option = document.createElement('option');
        option.value = year;
        option.textContent = year;
        select.appendChild(option);
    }
}

daySelects.forEach(select => {
    populateDays(select, currentMonth, currentYear);
    select.value = currentDay;
});
monthSelects.forEach(select => {
    populateMonths(select);
    select.value = currentMonth;
});
yearSelects.forEach(select => {
    populateYears(select);
    select.value = currentYear;
});

monthSelects.forEach((select, index) => {
    select.addEventListener('change', function () {
        const selectedMonth = parseInt(select.value);
        const selectedYear = parseInt(yearSelects[index].value);
        populateDays(daySelects[index], selectedMonth, selectedYear);
    });
});

yearSelects.forEach((select, index) => {
    select.addEventListener('change', function () {
        const selectedMonth = parseInt(monthSelects[index].value);
        const selectedYear = parseInt(select.value);
        populateDays(daySelects[index], selectedMonth, selectedYear);
    });
});

function UpdateProject(thisdata) {
    var currentTds = $("input[name='Iditem']:checked").closest('tr').find('td');
    var id = $("input[name='Iditem']:checked").closest('tr').find("input[name='Iditem']").val();
    var project = $(currentTds).eq(2).text();
    var status = $(currentTds).eq(3).text();
    $('.id-Edit').val(id);
    $('.report-name-edit').val(project);
    $(".status-edit").val(status.trim()).change();
}
$(function () {
    $('#t2 thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#t2 thead');
    var table = $('#t2').DataTable({
        "scrollX": true,
        scrollY: '70vh',
        scrollCollapse: true,
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        orderCellsTop: true,
        fixedHeader: true,
        info: false,
        order: [[1, 'desc']],
        dom: 'Bfrtip', // Hiển thị các nút export
        buttons: [
            'copy', 'csv', 'xlsx', 'pdf', 'print' // Các loại nút export
        ],
        "ajax": {
            "url": "/Project/LoadDataUserManager",
            "type": "Get",
            "dataSrc": ""
        },
        "columns": [
            {
                "data": "id", render: function (data, type, row) {
                    return '<span id="pinput"><input type="radio" onchange="UpdateProject(this)"  name="Iditem" id="Ischecked" value="' + data + '"><label for="itemno"></label></span>'
                        + '<a data-toggle="modal" data-target="#FormDetails" type="button" onclick="DetailPaymentOrder(this)" class="view" title="Detail" ></a>';
                }, "class": "text-center text-nowrap"
            },
            { "data": "id", "class": "text-nowrap" },
            { "data": "project", "class": "text-nowrap" },
            { "data": "status", "class": "text-nowrap" },
            { "data": "imported", "class": "text-nowrap" },
            { "data": "duplicate", "class": "text-nowrap" },
            { "data": "reviewed", "class": "text-nowrap" },
            { "data": "urls", "class": "text-nowrap" },
            { "data": "records", "class": "text-nowrap" },
            { "data": "revisit", "class": "text-nowrap" },
            { "data": "training", "class": "text-nowrap" }
        ],
        initComplete: function () {
            var api = this.api();
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    $(cell).html('<input type="text" class="inputSearch" placeholder="' + title + '" />');
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('keyup change', function (e) {
                            e.stopPropagation();
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();

                            var cursorPosition = this.selectionStart;
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();

                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });

    $(".btnSaveEdit").click(function () {
        var id = $(".id-Edit").val();
        var report = $('.report-name-edit').val();
        var status = $(".status-edit").val();
        $.ajax({
            url: '/Project/UpdateProject',
            type: "PUT",
            data: { idProject: id, project: report, status: status },
            success: function (result) {
                table.clear();
                table.ajax.reload(null, false);
                $(".btnCloseEdit").click();
            },
            error: function (err) {
                alert(err.statusText);
            }
        });
    });

    $(".file-name").change(function () {
        var fileName = $(this).val().split("\\").pop();
        $(".report-name").val(fileName);
    });
    $(".btnSave").click(function () {
        var reportName = $(".report-name").val();
        var reportTraing = $(".report-training").val();
        var formData = new FormData();
        formData.append("file", $(".file-name")[0].files[0]);
        formData.append("fileName", reportName);
        formData.append("training", reportTraing);
        $.ajax({
            url: '/Project/ImportFile',
            type: "POST",
            contentType: false, // Not to set any content header
            processData: false, // Not to process data
            data: formData,
            success: function (result) {
                table.clear();
                table.ajax.reload(null, false);
                $(".btnClose").click();
            },
            error: function (err) {
                alert(err.statusText);
            }
        });
    });
    $(".btnReportSummary").click(function () {
        var from = $(".year-from").val() + "-" + "0" + (parseInt($(".month-from").val()) + 1) + "-" + $(".day-from").val();
        var to = $(".year-to").val() + "-" + "0" + (parseInt($(".month-to").val()) + 1) + "-" + $(".day-to").val();
        $.ajax({
            url: '/Project/ReportSummary',
            type: "Get",
            data: { fromDate: from, toDate: to },
            success: function (result) {
                var wb = XLSX.utils.book_new();
                var ws = XLSX.utils.json_to_sheet(result);
                XLSX.utils.book_append_sheet(wb, ws, "Report Summary");
                XLSX.writeFile(wb, "report_summary.xlsx");
            },
            error: function (err) {
                alert(err.statusText);
            }
        });
    });
});