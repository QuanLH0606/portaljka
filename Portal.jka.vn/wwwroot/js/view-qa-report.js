﻿const daySelects = document.querySelectorAll('.daySelect');
        const monthSelects = document.querySelectorAll('.monthSelect');
        const yearSelects = document.querySelectorAll('.yearSelect');

        const currentDate = new Date();
        const currentDay = currentDate.getDate();
        const currentMonth = currentDate.getMonth();
        const currentYear = currentDate.getFullYear();

        function populateDays(select, month, year) {
            select.innerHTML = '';

            const daysInMonth = new Date(year, month + 1, 0).getDate();

            for (let day = 1; day <= daysInMonth; day++) {
                const option = document.createElement('option');
                option.value = day;
                option.textContent = day;
                select.appendChild(option);
            }
        }

        function populateMonths(select) {
            select.innerHTML = '';

            const months = [
                'January', 'February', 'March', 'April', 'May', 'June',
                'July', 'August', 'September', 'October', 'November', 'December'
            ];

            for (let month = 0; month < months.length; month++) {
                const option = document.createElement('option');
                option.value = month;
                option.textContent = months[month];
                select.appendChild(option);
            }
        }

        function populateYears(select) {
            const currentYear = new Date().getFullYear();

            for (let year = currentYear; year >= currentYear - 100; year--) {
                const option = document.createElement('option');
                option.value = year;
                option.textContent = year;
                select.appendChild(option);
            }
        }

        daySelects.forEach(select => {
            populateDays(select, currentMonth, currentYear);
            select.value = currentDay;
        });
        monthSelects.forEach(select => {
            populateMonths(select);
            select.value = currentMonth;
        });
        yearSelects.forEach(select => {
            populateYears(select);
            select.value = currentYear;
        });

        monthSelects.forEach((select, index) => {
            select.addEventListener('change', function () {
                const selectedMonth = parseInt(select.value);
                const selectedYear = parseInt(yearSelects[index].value);
                populateDays(daySelects[index], selectedMonth, selectedYear);
            });
        });

        yearSelects.forEach((select, index) => {
            select.addEventListener('change', function () {
                const selectedMonth = parseInt(monthSelects[index].value);
                const selectedYear = parseInt(select.value);
                populateDays(daySelects[index], selectedMonth, selectedYear);
            });
        });

        $(function () {
            $(".btnViewQA").click(function () {
                var selectedValue = $("input[name='Iditem']:checked").val();
                window.location.href = "/Project/ViewQAReport/?id=" + selectedValue;
            });
            var id = $(".projectId").val();
            $('#t2 thead tr')
                .clone(true)
                .addClass('filters')
                .appendTo('#t2 thead');
            var table = $('#t2').DataTable({
                "scrollX": true,
                scrollY: '70vh',
                scrollCollapse: true,
                "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
                orderCellsTop: true,
                fixedHeader: true,
                info: false,
                order: [[1, 'desc']],
                "ajax": {
                    "url": "/Project/LoadDataViewReport/?id=" + id,
                    "type": "Get",
                    "dataSrc": ""
                },
                "columns": [
                    { "data": "dateReviewed", "class": "text-nowrap" },
                    { "data": "legalBusinessMerchantName", "class": "text-nowrap" },
                    { "data": "billingDescriptor", "class": "text-nowrap" },
                    { "data": "merchantDBA", "class": "text-nowrap" },
                    { "data": "address1", "class": "text-nowrap" },
                    { "data": "address2", "class": "text-nowrap" },
                    { "data": "city", "class": "text-nowrap" },
                    { "data": "regionState", "class": "text-nowrap" },
                    { "data": "country", "class": "text-nowrap" },
                    { "data": "postalZipCode", "class": "text-nowrap" },
                    { "data": "phoneNumber", "class": "text-nowrap" },
                    { "data": "emailAddress", "class": "text-nowrap" },
                    { "data": "contactName", "class": "text-nowrap" },
                    { "data": "businessDescription", "class": "text-nowrap" },
                    { "data": "invalidUrls", "class": "text-nowrap" },
                    { "data": "validUrls", "class": "text-nowrap" },
                    { "data": "qaNote", "class": "text-nowrap" },
                ],
                initComplete: function () {
                    var api = this.api();
                    api
                        .columns()
                        .eq(0)
                        .each(function (colIdx) {
                            var cell = $('.filters th').eq(
                                $(api.column(colIdx).header()).index()
                            );
                            var title = $(cell).text();
                            $(cell).html('<input type="text" class="inputSearch" placeholder="' + title + '" />');
                            $(
                                'input',
                                $('.filters th').eq($(api.column(colIdx).header()).index())
                            )
                                .off('keyup change')
                                .on('keyup change', function (e) {
                                    e.stopPropagation();
                                    $(this).attr('title', $(this).val());
                                    var regexr = '({search})'; //$(this).parents('th').find('select').val();

                                    var cursorPosition = this.selectionStart;
                                    api
                                        .column(colIdx)
                                        .search(
                                            this.value != ''
                                                ? regexr.replace('{search}', '(((' + this.value + ')))')
                                                : '',
                                            this.value != '',
                                            this.value == ''
                                        )
                                        .draw();

                                    $(this)
                                        .focus()[0]
                                        .setSelectionRange(cursorPosition, cursorPosition);
                                });
                        });
                },
            });

        });