﻿$(function () {
    var id = $(".projectID").val();
    var currentElementIndex = 0;
    var result;

    function displayDataAtIndex(index) {
        var data = result[index];
        console.log(data.merchantDBA + data.city)
        $(".dataInforID").val(data.id);
        $(".merchanDBA").text(data.merchantDBA);
        $(".legalBusiness").text(data.legalBusiness);
        $(".billingDescriptor").text(data.billingDescriptor);
        $(".address1").text(data.address1);
        $(".suggestUrl").text(data.suggestedUrl);
        $(".address2").text(data.address2);
        $(".cityRegion").text(
            data.city + "/" + data.regionState + "/" + data.country + "/" + data.postalZipCode
        );
        $(".merchantDBACity").text(data.merchantDBA + " " + data.city);
        $(".legalBussinessCity").text(data.legalBusinessCity + " " + data.city);
        $(".phoneNumber").text(data.phoneNumber);
        $(".emailAddress").text(data.emailAddress);
        $(".contactName").text(data.contactName);
        var popupWindow = window.open(data.suggestedUrl, "popupWindowName", "width=800,height=1500");
    }

    $.ajax({
        url: "/Project/LoadDataQA",
        type: "GET",
        data: { projectID: id },
        success: function (response) {
            result = response;
            displayDataAtIndex(currentElementIndex);
        },
        error: function (error) {
            console.log("Error:", error);
        },
    });

    $(".btnExport").on("click", function () {
        currentElementIndex++;
        if (currentElementIndex < result.length) {
            displayDataAtIndex(currentElementIndex);
            var dataId = $(".dataInforID").val();
            $.ajax({
                url: "/Project/ExportData",
                type: "Post",
                data: { projectID: id, dataInforID: dataId },
                success: function (response) {
                }

            });
        } else {
            $.ajax({
                url: "/Project/UpdateUserReport",
                type: "Post",
                data: { projectID: id },
                success: function (response) {
                }
            });
            alert("All data displayed. Please continue with other data!");
            window.location.href = "/Project/ProjectProcessing";
        }
    });
    $(".btnSupervisor").click(function () {
        currentElementIndex++;
        if (currentElementIndex < result.length) {
            displayDataAtIndex(currentElementIndex);
            var dataId = $(".dataInforID").val();
            $.ajax({
                url: "/Project/SuppervisorCheck",
                type: "Post",
                data: { projectID: id, dataInforID: dataId },
                success: function (response) {
                }
            });
        }
    });
    $("#revisits").change(function () {
        var check = $(this).prop("checked");
        if (check) {
            currentElementIndex++;
            if (currentElementIndex < result.length) {
                displayDataAtIndex(currentElementIndex);
                var dataId = $(".dataInforID").val();
                $.ajax({
                    url: "/Project/RevisitCheck",
                    type: "Post",
                    data: { projectID: id, dataInforID: dataId },
                    success: function (response) {
                        $("#revisits").prop("checked", false);
                    }
                });
            }

        }
    })
    $(".btnSave").click(function () {
        var dataId = $(".dataInforID").val();
        var note = $(".analystNote").val();
        var url = $(".suggestUrl").val();
        var analystNote = {
            projectId: id,
            dataInforId: dataId,
            url: url,
            notes: note
        };
        $.ajax({
            url: "/Project/QASaveReport",
            type: "Post",
            data: { models: analystNote },
            success: function (response) {
                alert("Save data successfull!");
                console.log(response.suggestUrl);
                $(".suggestUrl").val(response.suggestUrl);

            }
        });
    });
})