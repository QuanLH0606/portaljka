﻿using Microsoft.EntityFrameworkCore;
using Portal.jka.vn.Domain;

namespace Portal.jka.vn.Data
{
    public class SeedDataDbContext
    {
        private readonly ApplicationDbContext _db;
        public SeedDataDbContext(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Initialize()
        {
            if (!_db.Users.Any())
            {
                var user = new User
                {
                    UserName = "admin",
                    Password = "123456",
                    FirstName = "admin",
                    LastName = "admin",
                    Email = "admin@portal.com",
                    IsActive = true,
                    RoleId = 1 
                };

                _db.Users.Add(user);
                _db.SaveChanges();
            }
            if (!_db.Roles.Any())
            {
                var listRole = new List<Role>
                {
                    new Role { Name = "Manager" },
                    new Role { Name = "QA" },
                    new Role { Name = "Check" },
                    new Role { Name = "Supervisor" },
                };
                foreach(var role in listRole)
                {
                    _db.Roles.Add(role);
                    _db.SaveChanges();
                }
            }

        }
    }
}
