﻿using Microsoft.EntityFrameworkCore;
using Portal.jka.vn.Domain;

namespace Portal.jka.vn.Data
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options)
        {
        }
        
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get;set; }
        public DbSet<Project> Projects { get;set; }
        public DbSet<DataInfor> DataInfors { get;set; }
        public DbSet<UserAssignData> UserAssignDatas { get;set; }
        public DbSet<ReportUserProject> ReportUserProjects { get;set; }
        public DbSet<Supervisor> Supervisors { get;set; }
        public DbSet<ExportData> ExportDatas { get;set; }
        public DbSet<QAData> QADatas { get;set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = 1,
                    UserName = "admin",
                    Password = "123456",
                    Email = "admin@portal.com"
                }
            );
        }
    }
}
