﻿using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.EntityFrameworkCore;
using Portal.jka.vn.Data;
using Portal.jka.vn.Domain;
using Portal.jka.vn.Models;
using System;
using System.Globalization;

namespace Portal.jka.vn.Services
{
    public class ProjectServices: IProjectServices
    {
        private readonly ApplicationDbContext _db;
        public ProjectServices(ApplicationDbContext db)
        {
            _db = db;
        }
        public async Task<Project> AddProjectAsync(IFormFile file, string fileName, bool training)
        {
            try
            {
                int totalRowCount = 0;
                int suggestedUrlRowCount = 0;

                using (var reader = new StreamReader(file.OpenReadStream()))
                using (var csvReader = new CsvReader(reader, new CsvConfiguration(CultureInfo.InvariantCulture)))
                {
                    csvReader.Read();
                    csvReader.ReadHeader();

                    while (csvReader.Read())
                    {
                        totalRowCount++;

                        string suggestedUrl = csvReader.GetField("suggested_url");

                        if (!string.IsNullOrWhiteSpace(suggestedUrl))
                        {
                            suggestedUrlRowCount++;
                        }
                    }

                }
                var project = new Project
                {
                    Reports = fileName,
                    Status = "Imported",
                    TotalRecord = totalRowCount,
                    RemainCheck = totalRowCount - suggestedUrlRowCount,
                    RemainSupervisor = 0,
                    Training = training
                };
                _db.Projects.Add(project);
                await _db.SaveChangesAsync();
                return project;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }
        public async Task<bool> AddDataInforAsync(IFormFile file, int projectID)
        {
            using (var reader = new StreamReader(file.OpenReadStream()))
            using (var csvReader = new CsvReader(reader, new CsvConfiguration(CultureInfo.InvariantCulture)))
            {
                csvReader.Read();
                csvReader.ReadHeader();

                while (csvReader.Read())
                {
                    string legalBussiness = csvReader.GetField("Legal Business/ Merchant Name");
                    string billingDescriptor = csvReader.GetField("Billing Descriptor");
                    string merchanDBA = csvReader.GetField("Merchant DBA");
                    string mcc = csvReader.GetField("MCC");
                    string id1 = csvReader.GetField("ID 1");
                    string masterCardICA = csvReader.GetField("MasterCard ICA");
                    string visaBin = csvReader.GetField("Visa BIN");
                    string address1 = csvReader.GetField("Address 1");
                    string address2 = csvReader.GetField("Address 2");
                    string city = csvReader.GetField("City");
                    string region = csvReader.GetField("Region/State");
                    string country = csvReader.GetField("Country");
                    string postalZipCode = csvReader.GetField("Postal/Zip Code");
                    string phoneNumber = csvReader.GetField("Phone Number");
                    string emailAddress = csvReader.GetField("Email Address");
                    string principalName = csvReader.GetField("Principal Name");
                    string principalAddress1 = csvReader.GetField("Principal Address 1");
                    string principalAddress2 = csvReader.GetField("Principal Address 2");
                    string principalCity = csvReader.GetField("Principal City");
                    string principalRegionState = csvReader.GetField("Principal Region/State");
                    string principalCountry = csvReader.GetField("Principal Country");
                    string principalPortalZipCode = csvReader.GetField("Principal Postal/Zip Code");
                    string principalPhoneNumber = csvReader.GetField("Principal Phone Number");
                    string principalEmailAddress = csvReader.GetField("Principal Email Address");
                    string principalMerchantContactName = csvReader.GetField("Primary Merchant Contact Name");
                    string productServiceDesciption = csvReader.GetField("Products/ Services Description");
                    string dataField1 = csvReader.GetField("Data Field 1");
                    string dataField2 = csvReader.GetField("Data Field 2");
                    string dataField3 = csvReader.GetField("Data Field 3");
                    string analyst = csvReader.GetField("Analyst");
                    string businessDescription = csvReader.GetField("Business Description");
                    string mdm = csvReader.GetField("MDM");
                    string originalMerchantDBA = csvReader.GetField("Original/Merchant DBA");
                    string contactName = csvReader.GetField("Contact Name");
                    string originalProductsServiceDesciption = csvReader.GetField("Original Products/ Services Description");
                    string originalEmailAddress = csvReader.GetField("Original Email Address");
                    string originalPrimaryMerchantContactName = csvReader.GetField("Original Primary Merchant Contact Name");
                    string originalLegalBusinessMerchantName = csvReader.GetField("Original Legal Business/ Merchant Name");
                    string originalBillingDescriptor = csvReader.GetField("Original Billing Descriptor");
                    string address3 = csvReader.GetField("Address 3");
                    string fax = csvReader.GetField("Fax");
                    string suggestedUrl = csvReader.GetField("suggested_url");
                    string pre1 = csvReader.GetField("Pre1");
                    string pre2 = csvReader.GetField("Pre2");
                    string pre3 = csvReader.GetField("Pre3");
                    string pre4 = csvReader.GetField("Pre4");
                    string pre5 = csvReader.GetField("Pre5");
                    string pre6 = csvReader.GetField("Pre6");
                    string pre7 = csvReader.GetField("Pre7");
                    string pre8 = csvReader.GetField("Pre8");
                    string pre9 = csvReader.GetField("Pre9");
                    string pre10 = csvReader.GetField("Pre10");
                    string pre11 = csvReader.GetField("Pre11");
                    string pre12 = csvReader.GetField("Pre12");
                    string pre13 = csvReader.GetField("Pre13");
                    string pre14 = csvReader.GetField("Pre14");
                    string pre15 = csvReader.GetField("Pre15");
                    var dataInfo = new DataInfor
                    {
                        ProjectId = projectID,
                        LegalBusiness = legalBussiness,
                        BillingDescriptor = billingDescriptor,
                        MerchantDBA = merchanDBA,
                        MCC = mcc,
                        ID1 = id1,
                        MasterCardICA = masterCardICA,
                        VisaBIN = visaBin,
                        Address1 = address1,
                        Address2 = address2,
                        City = city,
                        RegionState = region,
                        Country = country,
                        PostalZipCode = postalZipCode,
                        PhoneNumber = phoneNumber,
                        EmailAddress = emailAddress,
                        PrincipalName = principalName,
                        PrincipalAddress1 = principalAddress1,
                        PrincipalAddress2 = principalAddress2,
                        PrincipalCity = principalCity,
                        PrincipalRegionState = principalRegionState,
                        PrincipalCountry = principalCountry,
                        PrincipalPostalZipCode = principalPortalZipCode,
                        PrincipalPhoneNumber = principalPhoneNumber,
                        PrincipalEmailAddress = principalEmailAddress,
                        PrimaryMerchantContactName = principalMerchantContactName,
                        ProductsServicesDescription = productServiceDesciption,
                        DataField1 = dataField1,
                        DataField2 = dataField2,
                        DataField3 = dataField3,
                        Analyst = analyst,
                        BusinessDescription = businessDescription,
                        MDM = mdm,
                        OriginalMerchantDBA = originalMerchantDBA,
                        ContactName = contactName,
                        OriginalProductsServicesDescription = originalProductsServiceDesciption,
                        OriginalEmailAddress = originalEmailAddress,
                        OriginalPrimaryMerchantContactName = originalPrimaryMerchantContactName,
                        OriginalLegalBusinessMerchantName = originalLegalBusinessMerchantName,
                        OriginalBillingDescriptor = originalBillingDescriptor,
                        Address3 = address3,
                        Fax = fax,
                        SuggestedUrl = suggestedUrl,
                        Pre1 = pre1,
                        Pre2 = pre2,
                        Pre3 = pre3,
                        Pre4 = pre4,
                        Pre5 = pre5,
                        Pre6 = pre6,
                        Pre7 = pre7,
                        Pre8 = pre8,
                        Pre9 = pre9,
                        Pre10 = pre10,
                        Pre11 = pre11,
                        Pre12 = pre12,
                        Pre13 = pre13,
                        Pre14 = pre14,
                        Pre15 = pre15
                    };
                    _db.DataInfors.Add(dataInfo);
                    await _db.SaveChangesAsync();
                }

            }
            return true;
        }
        public async Task<bool> LoadUserProjectAsync(int id, string userName)
        {
            var dataInfor = await _db.DataInfors.Where(d => d.ProjectId == id).Take(20).ToListAsync();
            var reportUserProject = await _db.ReportUserProjects.FirstOrDefaultAsync(r => r.ProjectId == id && r.UserReport == userName && r.To == DateTime.MinValue);
            if (reportUserProject == null)
            {
                var reportUser = new ReportUserProject
                {
                    ProjectId = id,
                    From = DateTime.UtcNow,
                    NumberRecord = dataInfor.Count(),
                    UserReport = userName
                };
                _db.ReportUserProjects.Add(reportUser);
                await _db.SaveChangesAsync();
            }

            foreach (var data in dataInfor)
            {
                var userAssignData = await _db.UserAssignDatas.FirstOrDefaultAsync(u => u.DataInforsId == data.Id && u.UserAssign == userName);
                if (userAssignData == null)
                {
                    var userAssign = new UserAssignData
                    {
                        DataInforsId = data.Id,
                        UserAssign = userName,
                        ProjectId = id
                    };
                    _db.UserAssignDatas.Add(userAssign);
                    await _db.SaveChangesAsync();
                }
            }
            return true;
        }
        public async Task<SummaryWorkingProject> LoadDataInforAsync(int projectId, string username)
        {
            var userAssign = await _db.UserAssignDatas.Where(u => u.UserAssign == username && u.ProjectId == projectId).ToListAsync();
            var listDataInfor = new List<DataInfor>();
            DateTime now = DateTime.UtcNow;
            DateTime oneHourAgo = now.AddHours(-1);

            foreach (var dataUser in userAssign)
            {
                var dataInfor = await _db.DataInfors.FirstOrDefaultAsync(d => d.Id == dataUser.DataInforsId);
                listDataInfor.Add(dataInfor);
            }
            var summary = new SummaryWorkingProject
            {
                DataInfors = listDataInfor,
                ExportDatas = await _db.ExportDatas.Where(e => e.ProjectId == projectId && e.UserExport == username).CountAsync(),
                TotalDataInfors = await _db.DataInfors.Where(d => d.ProjectId == projectId).CountAsync(),
                CompleteToday = await _db.ExportDatas.Where(e => e.UserExport == username && e.CreateOn.Value.Date == (DateTime.UtcNow).Date).CountAsync(),
                CompleteCurrentProject = await _db.ExportDatas.Where(e => e.CreateOn.Value.Date == (DateTime.UtcNow).Date).CountAsync(),
                RatePerHours = await _db.ExportDatas.Where(e => e.UserExport == username && e.CreateOn >= oneHourAgo && e.CreateOn <= now).CountAsync()
            };
            return summary;
        }
        public async Task<SummaryWorkingProject> LoadDataSupervisorAsync(int projectId, string username)
        {
            var supervisor = await _db.Supervisors.Where(s => s.ProjectId == projectId && s.ModifiedBy == null).Take(20).ToListAsync();
            var listDataInfor = new List<DataInfor>();
            if (supervisor.Count() != 0)
            {
                foreach (var model in supervisor)
                {
                    var data = await _db.Supervisors.FirstOrDefaultAsync(s => s.ProjectId == projectId && s.ModifiedBy == null);
                    data.ModifiedBy = username;
                    _db.Supervisors.Update(data);
                    await _db.SaveChangesAsync();
                };
            }
            var dataSuppervisor = await _db.Supervisors.Where(s => s.ProjectId == projectId && s.ModifiedBy == username).Take(20).ToListAsync();
            foreach (var dataUser in dataSuppervisor)
            {
                var dataInfor = await _db.DataInfors.FirstOrDefaultAsync(d => d.Id == dataUser.DataInforId && d.ProjectId == dataUser.ProjectId);
                listDataInfor.Add(dataInfor);
            }
            var summary = new SummaryWorkingProject
            {
                DataInfors = listDataInfor,
                ExportDatas = await _db.ExportDatas.Where(e => e.ProjectId == projectId && e.UserExport == username).CountAsync(),
                TotalDataInfors = await _db.DataInfors.Where(d => d.ProjectId == projectId).CountAsync(),
                CompleteToday = await _db.ExportDatas.Where(e => e.UserExport == username && e.CreateOn.Value.Date == (DateTime.UtcNow).Date).CountAsync(),
                CompleteCurrentProject = await _db.ExportDatas.Where(e => e.CreateOn.Value.Date == (DateTime.UtcNow).Date).CountAsync(),
                RatePerHours = await _db.ExportDatas.Where(e => e.UserExport == username && e.CreateOn >= (DateTime.UtcNow).Date && e.CreateOn <= DateTime.UtcNow).CountAsync()
            };
            return summary;
        }
        public async Task<SummaryWorkingProject> ExportDataAsync(int projectId,int dataInforId, string username, string url)
        {
            var exportData = new ExportData
            {
                ProjectId = projectId,
                DataInforId = dataInforId,
                UserExport = username,
                Url = url
            };
            _db.ExportDatas.Add(exportData);
            await _db.SaveChangesAsync();
            var countDataExport = await _db.ExportDatas.Where(e => e.ProjectId == projectId && e.UserExport == username).ToListAsync();
            var summary = new SummaryWorkingProject
            {
                ExportDatas = await _db.ExportDatas.Where(e => e.ProjectId == projectId && e.UserExport == username).CountAsync(),
                CompleteToday = await _db.ExportDatas.Where(e => e.UserExport == username && e.CreateOn.Value.Date == (DateTime.UtcNow).Date).CountAsync(),
                CompleteCurrentProject = await _db.ExportDatas.Where(e => e.CreateOn.Value.Date == (DateTime.UtcNow).Date).CountAsync(),
                RatePerHours = await _db.ExportDatas.Where(e => e.UserExport == username && e.CreateOn >= (DateTime.UtcNow).AddHours(-1) && e.CreateOn <= DateTime.UtcNow).CountAsync()
            };
            return summary;
        }
        public async Task<ReportUserProject> UpdateUserReportAsync(int projectId, string username)
        {
            var reportUserProject = await _db.ReportUserProjects.FirstOrDefaultAsync(r => r.ProjectId == projectId && r.UserReport == username && r.To == DateTime.MinValue);
            reportUserProject.To = DateTime.UtcNow;
            _db.ReportUserProjects.Update(reportUserProject);
            await _db.SaveChangesAsync();
            return reportUserProject;
        }
        public async Task<Supervisor> SupervisorCheckAsync(int projectId, int dataInforID, string username)
        {
            var supervisor = new Supervisor
            {
                ProjectId = projectId,
                DataInforId = dataInforID,
                UserCheck = username
            };
            _db.Supervisors.Add(supervisor);
            await _db.SaveChangesAsync();
            return supervisor;
        }
        public async Task<QAData>QADataAsync(int projectId, int dataInforID, string username)
        {
            var qaData = new QAData
            {
                ProjectId = projectId,
                DataInforId = dataInforID,
                UserSend = username
            };
            _db.QADatas.Add(qaData);
            await _db.SaveChangesAsync();
            return qaData;
        }
        public async Task<List<DataInfor>> ListDataInforAsync(int projectId, string username)
        {
            var listDataInfor = new List<DataInfor>();
            var dataQA = await _db.QADatas.Where(q => q.ProjectId == projectId && q.ModifiedBy == null).Take(20).ToListAsync();
            if (dataQA.Count() != 0)
            {
                foreach (var data in dataQA)
                {
                    data.ModifiedBy = username;
                    _db.QADatas.Update(data);
                    await _db.SaveChangesAsync();
                }
            }
            var dataQAModified = await _db.QADatas.Where(q => q.ProjectId == projectId && q.ModifiedBy == username).Take(20).ToListAsync();
            foreach (var dataUser in dataQAModified)
            {
                var dataInfor = await _db.DataInfors.FirstOrDefaultAsync(d => d.Id == dataUser.DataInforId && d.ProjectId == dataUser.ProjectId);
                listDataInfor.Add(dataInfor);
            }
            return listDataInfor;
        }
        public async Task<DataInfor> QASaveReportAsync(AnalystNote models, string username)
        {
            var dataInfor = await _db.DataInfors.FirstOrDefaultAsync(d => d.Id == models.DataInforId && d.ProjectId == models.ProjectId);
            var qaData = await _db.QADatas.FirstOrDefaultAsync(q => q.DataInforId == models.DataInforId && q.ProjectId == models.ProjectId);
            dataInfor.SuggestedUrl = models.Url;
            qaData.AnalystNote = models.Notes;
            _db.DataInfors.Update(dataInfor);
            _db.QADatas.Update(qaData);
            await _db.SaveChangesAsync();
            return dataInfor;
        }
        public async Task<List<Statistics>> ListDataStatisticAsync()
        {
            var listDataStatistic = new List<Statistics>();
            var listProject = await _db.Projects.ToListAsync();
            foreach (var project in listProject)
            {
                var maxCreateOn = await _db.ExportDatas
                    .Where(e => e.ProjectId == project.Id)
                    .MaxAsync(e => e.CreateOn);

                var minCreateOn = await _db.ExportDatas
                    .Where(e => e.ProjectId == project.Id)
                    .MinAsync(e => e.CreateOn);
                var review = await _db.ExportDatas.Where(e => e.ProjectId == project.Id).CountAsync();
                var found = await _db.ExportDatas.Where(e => e.ProjectId == project.Id && e.Url != null).CountAsync();
                var recordHours = await _db.ExportDatas.Where(e => e.ProjectId == project.Id && e.CreateOn >= (DateTime.UtcNow).Date && e.CreateOn <= DateTime.UtcNow).CountAsync();
                var qad = await _db.QADatas.Where(q => q.ProjectId == project.Id).CountAsync();
                var falseNeg = await _db.ExportDatas.Where(e => e.Url == null).CountAsync();
                var validUrl = await _db.QADatas.Where(q => q.ProjectId == project.Id).Select(q => q.ValidUrl).ToListAsync();
                var falsePos = await _db.ExportDatas.Where(e => e.ProjectId == project.Id && !validUrl.Contains(e.Url)).CountAsync();
                var statistic = new Statistics
                {
                    Id = project.Id,
                    Report = project.Reports,
                    Reviewed = review,
                    Found = found,
                    Percent = found / review,
                    TimeSpent = (TimeSpan)(maxCreateOn - minCreateOn),
                    RecordHours = recordHours,
                    QAd = qad,
                    FalseNeg = falseNeg,

                };
                listDataStatistic.Add(statistic);
            }
            return listDataStatistic;
        }
        public async Task<ViewQAReport> ViewQAReportAsync(int id)
        {
            var qaData = await _db.QADatas.Where(q => q.ProjectId == id).ToListAsync();
            var viewQA = new ViewQAReport();
            viewQA.Id = id;
            viewQA.QAd = qaData.Count();
            viewQA.FalseNeg = 0;
            viewQA.FalsePos = 0;
            viewQA.Accuracy = 0;
            foreach (var qa in qaData)
            {
                viewQA.FalseNeg = await _db.ExportDatas.Where(e => e.Url == null || e.Url != qa.ValidUrl && e.ProjectId == id).CountAsync();
                viewQA.FalsePos = await _db.ExportDatas.Where(e => e.Url != qa.ValidUrl && e.ProjectId == id).CountAsync();
                viewQA.Accuracy = (await _db.ExportDatas.Where(e => e.Url == qa.ValidUrl && e.ProjectId == id).CountAsync()) / qaData.Count();
            }
            return viewQA;
        }
        public async Task<List<RecordQA>>LoadDataViewReportAsync(int id)
        {
            var recordQA = new List<RecordQA>();
            var models = await _db.QADatas.Where(q => q.ProjectId == id).ToListAsync();
            foreach (var item in models)
            {
                var dataInfor = await _db.DataInfors.FirstOrDefaultAsync(d => d.Id == item.DataInforId && d.ProjectId == item.ProjectId);
                var record = new RecordQA
                {
                    DateReviewed = (DateTime)dataInfor.CreateOn,
                    LegalBusinessMerchantName = dataInfor.LegalBusiness,
                    BillingDescriptor = dataInfor.BillingDescriptor,
                    MerchantDBA = dataInfor.MerchantDBA,
                    Address1 = dataInfor.Address1,
                    Address2 = dataInfor.Address2,
                    City = dataInfor.City,
                    RegionState = dataInfor.RegionState,
                    Country = dataInfor.Country,
                    PostalZipCode = dataInfor.PostalZipCode,
                    PhoneNumber = dataInfor.PhoneNumber,
                    EmailAddress = dataInfor.EmailAddress,
                    ContactName = dataInfor.ContactName,
                    BusinessDescription = dataInfor.BusinessDescription,
                    InvalidUrls = item.InValidUrl,
                    ValidUrls = item.ValidUrl,
                    QANote = item.AnalystNote
                };
                recordQA.Add(record);
            }
            return recordQA;
        }
        public async Task<List<RecordQA>> ViewUserSummaryAsync(int id, string username)
        {
            var recordQA = new List<RecordQA>();
            var models = await _db.ExportDatas.Where(q => q.ProjectId == id && q.UserExport == username).ToListAsync();
            foreach (var item in models)
            {
                var dataInfor = await _db.DataInfors.FirstOrDefaultAsync(d => d.Id == item.DataInforId && d.ProjectId == item.ProjectId);
                var qaData = await _db.QADatas.FirstOrDefaultAsync(q => q.ProjectId == id && q.DataInforId == dataInfor.Id);
                var note = qaData?.AnalystNote;
                var record = new RecordQA
                {
                    DateReviewed = (DateTime)dataInfor.CreateOn,
                    LegalBusinessMerchantName = dataInfor.LegalBusiness,
                    BillingDescriptor = dataInfor.BillingDescriptor,
                    MerchantDBA = dataInfor.MerchantDBA,
                    Address1 = dataInfor.Address1,
                    Address2 = dataInfor.Address2,
                    City = dataInfor.City,
                    RegionState = dataInfor.RegionState,
                    Country = dataInfor.Country,
                    PostalZipCode = dataInfor.PostalZipCode,
                    PhoneNumber = dataInfor.PhoneNumber,
                    EmailAddress = dataInfor.EmailAddress,
                    ContactName = dataInfor.ContactName,
                    BusinessDescription = dataInfor.BusinessDescription,
                    QANote = note
                };
                recordQA.Add(record);
            }
            return recordQA;
        }
        public async Task<List<ProjectQA>> LoadProjectQAAsync()
        {
            var project = await _db.Projects.ToListAsync();
            var projectQAs = new List<ProjectQA>();
            foreach (var model in project)
            {
                var duplicateCount = await _db.DataInfors.Where(d => d.ProjectId == model.Id)
                                    .GroupBy(d => new
                                    {
                                        d.LegalBusiness,
                                        d.BillingDescriptor,
                                        d.MerchantDBA,
                                        d.MCC,
                                        d.ID1,
                                        d.MasterCardICA,
                                        d.VisaBIN,
                                        d.Address1,
                                        d.Address2,
                                        d.City,
                                        d.RegionState,
                                        d.Country,
                                        d.PostalZipCode,
                                        d.PhoneNumber,
                                        d.EmailAddress,
                                        d.PrincipalName,
                                        d.PrincipalAddress1,
                                        d.PrincipalAddress2,
                                        d.PrincipalCity,
                                        d.PrincipalRegionState,
                                        d.PrincipalCountry,
                                        d.PrincipalPostalZipCode,
                                        d.PrincipalPhoneNumber,
                                        d.PrincipalEmailAddress,
                                        d.PrimaryMerchantContactName,
                                        d.ProductsServicesDescription,
                                        d.DataField1,
                                        d.DataField2,
                                        d.DataField3,
                                        d.Analyst,
                                        d.BusinessDescription,
                                        d.MDM,
                                        d.OriginalMerchantDBA,
                                        d.ContactName,
                                        d.OriginalProductsServicesDescription,
                                        d.OriginalEmailAddress,
                                        d.OriginalPrimaryMerchantContactName,
                                        d.OriginalLegalBusinessMerchantName,
                                        d.OriginalBillingDescriptor,
                                        d.Address3,
                                        d.Fax,
                                        d.SuggestedUrl,
                                        d.Pre1,
                                        d.Pre2,
                                        d.Pre3,
                                        d.Pre4,
                                        d.Pre5,
                                        d.Pre6,
                                        d.Pre7,
                                        d.Pre8,
                                        d.Pre9,
                                        d.Pre10,
                                        d.Pre11,
                                        d.Pre12,
                                        d.Pre13,
                                        d.Pre14,
                                        d.Pre15,
                                    })
                                    .Where(g => g.Count() > 1)
                                    .CountAsync();
                var projectQA = new ProjectQA
                {
                    Id = model.Id,
                    Project = model.Reports,
                    Imported = (DateTime)model.CreateOn,
                    Duplicate = duplicateCount,
                    Reviewed = await _db.ExportDatas.Where(e => e.ProjectId == model.Id).CountAsync() + await _db.Supervisors.Where(e => e.ProjectId == model.Id).CountAsync(),
                    Urls = await _db.DataInfors.Where(d => d.SuggestedUrl != null).CountAsync(),
                    Revisit = await _db.QADatas.Where(q => q.ProjectId == model.Id).CountAsync(),
                    Records = duplicateCount + await _db.ExportDatas.Where(e => e.ProjectId == model.Id).CountAsync() + await _db.Supervisors.Where(e => e.ProjectId == model.Id).CountAsync()
                };
                projectQAs.Add(projectQA);
            }
            return projectQAs;
        }
        public async Task<Statistics>ViewReportAsync(int id)
        {
            var project = await _db.Projects.FirstOrDefaultAsync(p => p.Id == id);
            var qaData = await _db.QADatas.Where(q => q.ProjectId == id).ToListAsync();
            var qaDataUrls = qaData.Select(q => q.ValidUrl).ToList();

            var maxExportCreateOn = await _db.ExportDatas
                   .Where(e => e.ProjectId == project.Id)
                   .MaxAsync(e => e.CreateOn);
            var minExportCreateOn = await _db.ExportDatas
                .Where(e => e.ProjectId == project.Id)
                .MinAsync(e => e.CreateOn);
            var maxSupervisorCreateOn = await _db.Supervisors
                  .Where(e => e.ProjectId == project.Id)
                  .MaxAsync(e => e.CreateOn);
            var minSupervosprCreateOn = await _db.Supervisors
                .Where(e => e.ProjectId == project.Id)
                .MinAsync(e => e.CreateOn);
            TimeSpan timeSpent = (TimeSpan)((maxExportCreateOn - minExportCreateOn) + (maxSupervisorCreateOn - minSupervosprCreateOn));
            var statistics = new Statistics
            {
                Id = project.Id,
                Report = project.Reports,
                Status = project.Status,
                Completed = await _db.ExportDatas.Where(e => e.ProjectId == project.Id).CountAsync() + await _db.Supervisors.Where(e => e.ProjectId == project.Id).CountAsync(),
                TimeSpent = timeSpent,
                RecordHours = (int)((await _db.ExportDatas.Where(e => e.ProjectId == project.Id).CountAsync() + await _db.Supervisors.Where(e => e.ProjectId == project.Id).CountAsync()) / timeSpent.TotalMinutes) * 60,
                QAd = await _db.QADatas.Where(q => q.ProjectId == project.Id).CountAsync(),
                FalsePos = await _db.ExportDatas.Where(e => !qaDataUrls.Contains(e.Url)).CountAsync(),
                FalseNeg = await _db.ExportDatas.Where(e => e.Url == null).CountAsync() + await _db.Supervisors.Where(e => e.Url == null).CountAsync(),
            };
            return statistics;
        }
        public async Task<List<ReportUserStatistics>>ReportUserAsync(int id, string username)
        {
            var reportUsers = new List<ReportUserStatistics>();
            var userCheckUsernames = await _db.ExportDatas
                                            .Where(e => e.ProjectId == id && e.UserExport == username)
                                            .Select(e => e.UserExport)
                                            .Distinct()
                                            .ToListAsync();
            foreach (var userCheck in userCheckUsernames)
            {
                var qaQuery = _db.QADatas.Where(q => q.ProjectId == id && q.UserSend == userCheck).Select(q => q.ValidUrl);
                var report = new ReportUserStatistics
                {
                    Username = userCheck,
                    Completed = await _db.ExportDatas.Where(e => e.ProjectId == id && e.UserExport == userCheck).CountAsync(),
                    Found = await _db.ExportDatas.Where(e => e.ProjectId == id && e.UserExport == userCheck && e.Url != null).CountAsync(),
                    Percent = (await _db.ExportDatas.Where(e => e.ProjectId == id && e.UserExport == userCheck && e.Url != null).CountAsync() / await _db.ExportDatas.Where(e => e.ProjectId == id && e.UserExport == userCheck).CountAsync()) * 100,
                    RecordHours = (int)(await _db.ExportDatas.Where(e => e.ProjectId == id && e.UserExport == userCheck).CountAsync() / 60),
                    QAd = await _db.QADatas.Where(q => q.ProjectId == id && q.UserSend == userCheck).CountAsync(),
                    FalsePos = await _db.ExportDatas.Where(e => e.ProjectId == id && !qaQuery.Contains(e.Url)).CountAsync(),
                    FalseNeg = await _db.ExportDatas.Where(e => e.ProjectId == id && !qaQuery.Contains(e.Url)).CountAsync(),
                    PercenQAd = (await _db.QADatas.Where(q => q.ProjectId == id && q.UserSend == userCheck).CountAsync() / await _db.DataInfors.Where(d => d.ProjectId == id).CountAsync()) * 100,
                    Accuracy = (await _db.ExportDatas.Where(e => e.ProjectId == id && qaQuery.Contains(e.Url)).CountAsync() / await _db.QADatas.Where(q => q.ProjectId == id).CountAsync()) * 100,
                    Revisit = await _db.QADatas.Where(e => e.ProjectId == id && e.UserSend == userCheck).CountAsync()
                };
                reportUsers.Add(report);
            }

            var userSupervisorUsernames = await _db.Supervisors
                .Where(e => e.ProjectId == id && e.ModifiedBy == username)
                .Select(e => e.ModifiedBy)
                .Distinct()
                .ToListAsync();
            foreach (var userSupervisor in userSupervisorUsernames)
            {
                var qaQuery = _db.QADatas.Where(q => q.ProjectId == id && q.UserSend == userSupervisor).Select(q => q.ValidUrl);
                var report = new ReportUserStatistics
                {
                    Username = userSupervisor,
                    Completed = await _db.Supervisors.Where(e => e.ProjectId == id && e.ModifiedBy == userSupervisor).CountAsync(),
                    Found = await _db.Supervisors.Where(e => e.ProjectId == id && e.ModifiedBy == userSupervisor && e.Url != null).CountAsync(),
                    Percent = (await _db.Supervisors.Where(e => e.ProjectId == id && e.ModifiedBy == userSupervisor && e.Url != null).CountAsync() / await _db.ExportDatas.Where(e => e.ProjectId == id && e.UserExport == userSupervisor).CountAsync()) * 100,
                    RecordHours = (int)(await _db.Supervisors.Where(e => e.ProjectId == id && e.ModifiedBy == userSupervisor).CountAsync() / 60),
                    QAd = await _db.QADatas.Where(q => q.ProjectId == id && q.UserSend == userSupervisor).CountAsync(),
                    FalsePos = await _db.Supervisors.Where(e => e.ProjectId == id && !qaQuery.Contains(e.Url)).CountAsync(),
                    FalseNeg = await _db.Supervisors.Where(e => e.ProjectId == id && !qaQuery.Contains(e.Url)).CountAsync(),
                    PercenQAd = (await _db.QADatas.Where(q => q.ProjectId == id && q.UserSend == userSupervisor).CountAsync() / await _db.DataInfors.Where(d => d.ProjectId == id).CountAsync()) * 100,
                    Accuracy = (await _db.Supervisors.Where(e => e.ProjectId == id && qaQuery.Contains(e.Url)).CountAsync() / await _db.QADatas.Where(q => q.ProjectId == id).CountAsync()) * 100,
                    Revisit = await _db.QADatas.Where(e => e.ProjectId == id && e.UserSend == userSupervisor).CountAsync()
                };
                reportUsers.Add(report);
            }
            return reportUsers;
        }
        public async Task<List<ProjectQA>> LoadDataUserManagerAsync()
        {
            var project = await _db.Projects.ToListAsync();
            var projectQAs = new List<ProjectQA>();
            foreach (var model in project)
            {
                var duplicateCount = await _db.DataInfors.Where(d => d.ProjectId == model.Id)
                                    .GroupBy(d => new
                                    {
                                        d.LegalBusiness,
                                        d.BillingDescriptor,
                                        d.MerchantDBA,
                                        d.MCC,
                                        d.ID1,
                                        d.MasterCardICA,
                                        d.VisaBIN,
                                        d.Address1,
                                        d.Address2,
                                        d.City,
                                        d.RegionState,
                                        d.Country,
                                        d.PostalZipCode,
                                        d.PhoneNumber,
                                        d.EmailAddress,
                                        d.PrincipalName,
                                        d.PrincipalAddress1,
                                        d.PrincipalAddress2,
                                        d.PrincipalCity,
                                        d.PrincipalRegionState,
                                        d.PrincipalCountry,
                                        d.PrincipalPostalZipCode,
                                        d.PrincipalPhoneNumber,
                                        d.PrincipalEmailAddress,
                                        d.PrimaryMerchantContactName,
                                        d.ProductsServicesDescription,
                                        d.DataField1,
                                        d.DataField2,
                                        d.DataField3,
                                        d.Analyst,
                                        d.BusinessDescription,
                                        d.MDM,
                                        d.OriginalMerchantDBA,
                                        d.ContactName,
                                        d.OriginalProductsServicesDescription,
                                        d.OriginalEmailAddress,
                                        d.OriginalPrimaryMerchantContactName,
                                        d.OriginalLegalBusinessMerchantName,
                                        d.OriginalBillingDescriptor,
                                        d.Address3,
                                        d.Fax,
                                        d.SuggestedUrl,
                                        d.Pre1,
                                        d.Pre2,
                                        d.Pre3,
                                        d.Pre4,
                                        d.Pre5,
                                        d.Pre6,
                                        d.Pre7,
                                        d.Pre8,
                                        d.Pre9,
                                        d.Pre10,
                                        d.Pre11,
                                        d.Pre12,
                                        d.Pre13,
                                        d.Pre14,
                                        d.Pre15,
                                    })
                                    .Where(g => g.Count() > 0)
                                    .CountAsync();
                var projectQA = new ProjectQA
                {
                    Id = model.Id,
                    Project = model.Reports,
                    Imported = (DateTime)model.CreateOn,
                    Duplicate = duplicateCount,
                    Reviewed = await _db.ExportDatas.Where(e => e.ProjectId == model.Id).CountAsync() + await _db.Supervisors.Where(e => e.ProjectId == model.Id).CountAsync(),
                    Urls = await _db.DataInfors.Where(d => d.SuggestedUrl != null).CountAsync(),
                    Revisit = await _db.QADatas.Where(q => q.ProjectId == model.Id).CountAsync(),
                    Records = duplicateCount + await _db.ExportDatas.Where(e => e.ProjectId == model.Id).CountAsync() + await _db.Supervisors.Where(e => e.ProjectId == model.Id).CountAsync(),
                    Status = model.Status,
                    Training = (bool)model.Training
                };
                projectQAs.Add(projectQA);
            }
            return projectQAs;
        }
        public async Task<Project> UpdateProjectAsync(int idProject, string project, string status)
        {
            var model = await _db.Projects.FirstOrDefaultAsync(p => p.Id == idProject);
            model.Reports = project;
            model.Status = status;
            _db.Projects.Update(model);
            await _db.SaveChangesAsync();
            return model;
        }
        public async Task<List<ExcelReportSummary>> ReportSummaryAsync(string fromDate, string toDate)
        {
            var users = await _db.Users.ToListAsync();
            var excelReportSummaries = new List<ExcelReportSummary>();
            DateTime startDate = DateTime.Parse(fromDate);
            DateTime endDate = DateTime.Parse(toDate);

            DateTime startUtc = startDate.ToUniversalTime();
            DateTime endUtc = endDate.ToUniversalTime();
            foreach (var user in users)
            {
                var role = await _db.Roles.FirstOrDefaultAsync(r => r.Id == user.RoleId);
                var exportData = await _db.ExportDatas.Where(e => e.UserExport == user.UserName && e.CreateOn >= startUtc && e.CreateOn <= endUtc).CountAsync();
                var qaData = await _db.QADatas.Where(e => e.ModifiedBy == user.UserName && e.CreateOn >= startUtc && e.CreateOn <= endUtc).CountAsync();
                var supervisor = await _db.Supervisors.Where(e => e.ModifiedBy == user.UserName && e.CreateOn >= startUtc && e.CreateOn <= endUtc).CountAsync();
                var excelReportSummary = new ExcelReportSummary
                {
                    User = user.UserName,
                    Completed = exportData + qaData + supervisor,
                    Role = role.Name
                };
                excelReportSummaries.Add(excelReportSummary);
            }
            return excelReportSummaries;
        }
    }
}
