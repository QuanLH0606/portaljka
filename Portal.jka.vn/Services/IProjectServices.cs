﻿using Portal.jka.vn.Domain;
using Portal.jka.vn.Models;

namespace Portal.jka.vn.Services
{
    public interface IProjectServices
    {
        public Task<Project> AddProjectAsync(IFormFile file, string fileName, bool training);
        public Task<bool> AddDataInforAsync(IFormFile file, int projectID);
        public Task<bool> LoadUserProjectAsync(int id, string userName);
        public Task<SummaryWorkingProject> LoadDataInforAsync(int projectId, string username);
        public Task<SummaryWorkingProject> LoadDataSupervisorAsync(int projectId, string username);
        public Task<SummaryWorkingProject> ExportDataAsync(int projectId, int dataInforId, string username, string url);
        public Task<ReportUserProject> UpdateUserReportAsync(int projectId, string username);
        public Task<Supervisor> SupervisorCheckAsync(int projectId, int dataInforID, string username);
        public Task<QAData> QADataAsync(int projectId, int dataInforID, string username);
        public Task<List<DataInfor>> ListDataInforAsync(int projectId, string username);
        public Task<DataInfor> QASaveReportAsync(AnalystNote models, string username);
        public Task<List<Statistics>> ListDataStatisticAsync();
        public Task<ViewQAReport> ViewQAReportAsync(int id);
        public Task<List<RecordQA>> LoadDataViewReportAsync(int id);
        public Task<List<RecordQA>> ViewUserSummaryAsync(int id, string username);
        public Task<List<ProjectQA>> LoadProjectQAAsync();
        public Task<Statistics> ViewReportAsync(int id);
        public Task<List<ReportUserStatistics>> ReportUserAsync(int id, string username);
        public Task<List<ProjectQA>> LoadDataUserManagerAsync();
        public Task<Project> UpdateProjectAsync(int idProject, string project, string status);
        public Task<List<ExcelReportSummary>> ReportSummaryAsync(string fromDate, string toDate);
    }
}
