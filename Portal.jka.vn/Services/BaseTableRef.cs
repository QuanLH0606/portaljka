﻿namespace Portal.jka.vn.Services
{
    public class BaseTableRef
    {
        public string? CreatedBy { get; set; }
        public DateTime? CreateOn { get; set; } = DateTime.UtcNow;
        public string? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set;} = DateTime.UtcNow;
        public string? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }

    }
}
