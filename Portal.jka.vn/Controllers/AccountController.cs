﻿using Microsoft.AspNetCore.Mvc;

namespace Portal.jka.vn.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("UserEmail");

            return RedirectToAction("Index", "Home");
        }
    }
}
