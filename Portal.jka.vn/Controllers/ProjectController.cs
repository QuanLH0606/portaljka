﻿using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Portal.jka.vn.Data;
using Portal.jka.vn.Domain;
using Portal.jka.vn.Models;
using Portal.jka.vn.Services;
using System;
using System.Formats.Asn1;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Portal.jka.vn.Controllers
{
    public class ProjectController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IProjectServices _project;
        public ProjectController(ApplicationDbContext db, IProjectServices project)
        {
            _db = db;
            _project = project;
        }
        public IActionResult Index()
        {
            string username = HttpContext.Session.GetString("UserEmail");
            if(username == null)
            {
                return RedirectToAction("Index","Home");
            }
            return View();
        }
        public IActionResult ProjectProcessing()
        {
            return View();
        }
        public IActionResult QALogin()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ImportFile(IFormFile file, string fileName, bool training)
        {
            var reports = await _db.Projects.ToListAsync();
            var check = reports.FirstOrDefault(r => r.Reports == file.FileName);
            if (check == null) 
            {
                if (file != null && file.Length > 0)
                {
                    var project = await _project.AddProjectAsync(file, fileName, training);
                    var dataInfor = await _project.AddDataInforAsync(file, project.Id);
                    return Json(new { project });
                }
                return BadRequest("Không có file được tải lên.");
            }
            else
            {
                return Json("File đã tồn tại trên hệ thống!");
            }


        }
        
        [HttpGet]
        public async Task<IActionResult> LoadDataProject()
        {
            var project = await _db.Projects.ToListAsync();
            return Json(project);
        }

        [Route("project/check/{id:int}")]
        public async Task<IActionResult> CheckReport(int id)
        {
            var project = await _db.Projects.FirstOrDefaultAsync(p => p.Id == id);
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            if (user.RoleId == 4)
            {
                return RedirectToAction("SupervisorReport","Project", new { projectID = id});
            }
            var loadData = await _project.LoadUserProjectAsync(project.Id, user.UserName);
            return View(project);

        }

        public async Task<IActionResult> SupervisorReport(int projectID)
        {
            var project = await _db.Projects.FirstOrDefaultAsync(p => p.Id == projectID);
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            return View(project);
        }

        public async Task<IActionResult> Statistics() 
        {
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            return View(user);
        }

        [HttpGet]
        public async Task<IActionResult> LoadDataInfors(int projectID)
        {
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            var summary = await _project.LoadDataInforAsync(projectID, user.UserName);
            return Json(summary);
        }

        [HttpGet]
        public async Task<IActionResult> LoadDataSupervisors(int projectID)
        {
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            var summary = await _project.LoadDataSupervisorAsync(projectID, user.UserName);
            return Json(summary);
        }


        [HttpPost]
        public async Task<IActionResult> ExportData(int projectID, int dataInforID, string url)
        {
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            var summary = await _project.ExportDataAsync(projectID, dataInforID, user.UserName, url);
            return Json(summary);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUserReport(int projectID)
        {
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            var reportUserProject = await _project.UpdateUserReportAsync(projectID, user.UserName);
            return Json(reportUserProject);
        }

        [HttpPost]
        public async Task<IActionResult> SuppervisorCheck(int projectID, int dataInforID)
        {
            try
            {
                string username = HttpContext.Session.GetString("UserEmail");
                var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
                var supervisor = await _project.SupervisorCheckAsync(projectID, dataInforID, user.UserName);
                return Json(supervisor);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        [HttpPost]
        public async Task<IActionResult> RevisitCheck(int projectID, int dataInforID)
        {
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            var qaData = await _project.QADataAsync(projectID, dataInforID, user.UserName);
            return Json(qaData);
        }

        public async Task<IActionResult> QAReport()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult>LoadDataQA(int projectID)
        {
            try
            {
                string username = HttpContext.Session.GetString("UserEmail");
                var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
                var listDataInfor = await _project.ListDataInforAsync(projectID, user.UserName);
                return Json(listDataInfor);
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> QASaveReport (AnalystNote models)
        {
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            var dataInfor = await _project.QASaveReportAsync(models, user.UserName);
            return Json(dataInfor);
        }

        [HttpGet]
        public async Task<IActionResult> LoadDataStatistic()
        {
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            try
            {
                var listDataStatistic = await _project.ListDataStatisticAsync();
                return Json(listDataStatistic);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }
        public async Task<IActionResult> ViewQAReport(int id)
        {
            var viewQA = await _project.ViewQAReportAsync(id);
            return View(viewQA);
        }

        [HttpGet]
        public async Task<IActionResult> LoadDataViewReport(int id) 
        {
            var recordQA = await _project.LoadDataViewReportAsync(id);
            return Json(recordQA);
        }
        public async Task<IActionResult> ViewUserSummary(int id)
        {
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            var project = await _db.Projects.FirstOrDefaultAsync(p => p.Id == id);
            var userProject = new UserProject
            {
                User = user,
                Project = project
            };
            return View(userProject);
        }
        [HttpGet]
        public async Task<IActionResult> LoadDataViewUserSummary(int id)
        {
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            var recordQA = await _project.ViewUserSummaryAsync(id, user.UserName);
            return Json(recordQA);

        }

        public IActionResult ProjectQA()
        {
            return View();
        }
        
        public IActionResult UserStatistics()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> LoadProjectQA()
        {
            var projectQAs = await _project.LoadProjectQAAsync();
            return Json(projectQAs);
        }

        public async Task<IActionResult> ViewReport(int id)
        {
            var statistics = await _project.ViewReportAsync(id);
            return View(statistics);
        }

        [HttpGet]
        public async Task<IActionResult> ReportUserProject(int id)
        {
            try
            {
                string username = HttpContext.Session.GetString("UserEmail");
                var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
                var reportUsers = await _project.ReportUserAsync(id, user.UserName);
                
                return Json(reportUsers);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
           
        }

        public IActionResult QA()
        {
            return View();
        }
        public IActionResult ActiveWorksheet() 
        {
            return View();
        }
        public IActionResult AnalystStatistics()
        {
            return View();
        }
        public IActionResult QAStatistics()
        {
            return View();
        }
        public IActionResult ViewUser()
        {
            return View();
        }
        public IActionResult UserManager()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> LoadDataUserManager()
        {
            var projectQAs = await _project.LoadDataUserManagerAsync();
            return Json(projectQAs);
        }
        [HttpPut]
        public async Task<IActionResult> UpdateProject(int idProject,string project, string status)
        {
            var model = await _project.UpdateProjectAsync(idProject, project, status);
            
            return Json(model);
        }
        [HttpGet]
        public async Task<IActionResult> ReportSummary(string fromDate, string toDate)
        {
            try
            {
                var excelReportSummaries = await _project.ReportSummaryAsync(fromDate, toDate);
              
                return Json(excelReportSummaries);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }
    }
}
 