﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Portal.jka.vn.Data;
using Portal.jka.vn.Domain;
using Portal.jka.vn.Models;

namespace Portal.jka.vn.Controllers
{
    public class UserController : Controller
    {
        private readonly ApplicationDbContext _db;
        public UserController(ApplicationDbContext db)
        {
            _db = db;
        }
        public async Task<IActionResult> List()
        {
            string username = HttpContext.Session.GetString("UserEmail");
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == username);
            if (user.RoleId == 1)
            {
                var role = await _db.Roles.ToListAsync();
                return View(role);
            }
            else
            {
                return RedirectToAction("Index","Project");
            }
  
        }

        [HttpGet]
        public async Task<IActionResult> GetList()
        {
            List<UserRoles> userRoles = new List<UserRoles>(); 
            var listUser = await _db.Users.ToListAsync();
            foreach(var user in listUser)
            {
                var role = await _db.Roles.FirstOrDefaultAsync(r => r.Id == user.RoleId);
                var userRole = new UserRoles
                {
                    User = user,
                    Role = role
                };
                userRoles.Add(userRole);
            }
            return Json(userRoles);
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] User userRequest)
        {
            await _db.Users.AddAsync(userRequest);
            await _db.SaveChangesAsync();
            return Json(new { userRequest });
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUser([FromBody] User userUpdate)
        {
            var models = await _db.Users.FirstOrDefaultAsync(m => m.Id == userUpdate.Id);

            if (models == null)
            {
                return NotFound(); 
            }

            models.UserName = userUpdate.UserName;
            models.Email = userUpdate.Email;
            models.Password = userUpdate.Password;
            models.RoleId = userUpdate.RoleId;

            _db.Entry(models).State = EntityState.Modified;

            await _db.SaveChangesAsync(); 

            return Json(1); 
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var model = await _db.Users.FirstOrDefaultAsync(m => m.Id == id);

            if (model == null)
            {
                return NotFound(); // User not found
            }

            _db.Users.Remove(model); // Mark the entity as removed

            await _db.SaveChangesAsync(); // Save changes

            return Json(1); // Successful delete
        }

        [HttpGet]
        public async Task<IActionResult> GetUserId (int id)
        {
            var user = await _db.Users.FirstOrDefaultAsync(x => x.Id == id);
            return Json(user);
        }
    }
}
