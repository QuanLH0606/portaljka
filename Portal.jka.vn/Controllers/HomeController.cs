﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Portal.jka.vn.Data;
using Portal.jka.vn.Models;
using System.Diagnostics;

namespace Portal.jka.vn.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext db)
        {
            _logger = logger;
            _db = db;
        }

        public async Task<IActionResult> Index()
        {
            string username = HttpContext.Session.GetString("UserEmail");
            if (username != null)
            {
                return RedirectToAction("Index", "Project");
            }
            var roles = await _db.Roles.ToListAsync();
            return View(roles);
        }

        public async Task<IActionResult> Login()
        {
            string email = Request.Form["email"].ToString();
            string password = Request.Form["password"].ToString();
            string roles = Request.Form["roleId"].ToString();
            var user = await _db.Users.Where(u => u.Email == email && u.Password == password && u.RoleId == int.Parse(roles) && u.IsActive==true)
                                 .FirstOrDefaultAsync();

                if (user != null)
                {
                    HttpContext.Session.SetString("UserEmail", user.Email);
                    if (user.RoleId == 1)
                    {
                        return RedirectToAction("UserManager", "Project");
                    }
                    if(user.RoleId == 2)
                    {
                        return RedirectToAction("QALogin", "Project");
                    }

                    return RedirectToAction("Index", "Project");
                }
                else
                {
                    TempData["ErrorMessage"] = "Tên đăng nhập hoặc mật khẩu không đúng.";
                    return RedirectToAction("Index", "Home");
                }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}