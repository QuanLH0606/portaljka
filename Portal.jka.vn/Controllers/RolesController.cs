﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Portal.jka.vn.Data;
using Portal.jka.vn.Domain;

namespace Portal.jka.vn.Controllers
{
    public class RolesController : Controller
    {
        private readonly ApplicationDbContext _db;
        public RolesController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult List()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> ListRole()
        {
            var models = await _db.Roles.ToListAsync();
            return Json(models);
        }
        [HttpPost]
        public async Task<IActionResult> CreateRoleAsync(Role role)
        {
            await _db.Roles.AddAsync(role);
            await _db.SaveChangesAsync();
            return View();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateRoleAsync(Role role)
        {
            var models = await _db.Roles.FirstOrDefaultAsync(m => m.Id == role.Id);
            if (models == null)
            {
                return NotFound();
            }

            models.Name = role.Name;
            models.Description = role.Description;
            _db.Entry(models).State = EntityState.Modified;

            await _db.SaveChangesAsync();
            return Json(1);
        }
        public async Task<IActionResult> DeleteRoleAsync(int id)
        {
            var models = await _db.Roles.FirstOrDefaultAsync(m => m.Id == id);
            _db.Roles.Remove(models);
            await _db.SaveChangesAsync();
            return Json(1);
        }
    }
}
