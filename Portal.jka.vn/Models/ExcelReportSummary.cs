﻿namespace Portal.jka.vn.Models
{
    public class ExcelReportSummary
    {
        public string User { get; set; }
        public int Completed { get; set; }
        public string Role { get; set; }
    }
}
