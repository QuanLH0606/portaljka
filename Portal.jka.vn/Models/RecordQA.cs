﻿namespace Portal.jka.vn.Models
{
    public class RecordQA
    {
        public DateTime DateReviewed { get; set; }
        public string LegalBusinessMerchantName { get; set; }
        public string BillingDescriptor { get; set; }
        public string MerchantDBA { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string RegionState { get; set; }
        public string Country { get; set; }
        public string PostalZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string ContactName { get; set; }
        public string BusinessDescription { get; set; }
        public string InvalidUrls { get; set; }
        public string ValidUrls { get; set; }
        public string QANote { get; set; }
    }
}
