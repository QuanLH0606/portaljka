﻿namespace Portal.jka.vn.Models
{
    public class ProjectQA
    {
        public int Id { get; set; }
        public string Project { get; set; }
        public DateTime Imported { get; set; }
        public int Duplicate { get; set; }
        public int Reviewed { get; set; }
        public int Urls { get; set; }
        public int Records { get; set; }
        public int Revisit { get; set; }
        public string Status { get; set; }
        public bool Training { get; set; }

    }
}
