﻿namespace Portal.jka.vn.Models
{
    public class Statistics
    {
        public int Id { get; set; }
        public string Report { get; set; }
        public int Reviewed { get; set; }
        public int Found { get; set; }
        public double Percent { get; set; }
        public TimeSpan TimeSpent { get; set; }
        public int RecordHours { get; set; }
        public int QAd { get; set; }
        public int FalsePos { get; set; }
        public int FalseNeg { get; set; }
        public double Accuracy { get; set; }
        public string Status { get; set; }
        public int Completed { get; set; }
    }
}
