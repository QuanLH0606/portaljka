﻿using Portal.jka.vn.Domain;

namespace Portal.jka.vn.Models
{
    public class UserProject
    {
        public User User { get; set; }
        public Project Project { get; set; }
    }
}
