﻿using Portal.jka.vn.Domain;

namespace Portal.jka.vn.Models
{
    public class SummaryWorkingProject
    {
        public List<DataInfor> DataInfors { get; set; }
        public int CompleteCurrentProject { get; set; }
        public int TotalDataInfors { get; set; }
        public int ExportDatas { get; set; }
        public int CompleteToday { get; set; }
        public int RatePerHours { get; set; }
    }
}
