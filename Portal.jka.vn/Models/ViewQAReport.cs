﻿namespace Portal.jka.vn.Models
{
    public class ViewQAReport
    {
        public int Id { get; set; }
        public int QAd { get; set; }
        public int FalsePos { get; set; }
        public int FalseNeg { get; set; }
        public double Accuracy { get; set; }
    }
}
