﻿namespace Portal.jka.vn.Models
{
    public class AnalystNote
    {
        public int ProjectId { get; set; }
        public int DataInforId { get; set; }
        public string Notes { get; set; }
        public string Url { get; set; }
    }
}
