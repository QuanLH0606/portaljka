﻿using Portal.jka.vn.Domain;

namespace Portal.jka.vn.Models
{
    public class UserRoles
    {
        public User User { get; set; }
        public Role Role { get; set; }
    }
}
