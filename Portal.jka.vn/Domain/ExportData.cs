﻿using Portal.jka.vn.Services;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.jka.vn.Domain
{
    [Table("ExportDatas", Schema = "Catalog")]
    public class ExportData: BaseTableRef
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int DataInforId { get; set; }
        public string UserExport { get; set; }
        public string? Url { get; set; }
    }
}
