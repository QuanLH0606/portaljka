﻿using Portal.jka.vn.Services;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.jka.vn.Domain
{
    [Table("UserAssignDatas", Schema = "Catalog")]
    public class UserAssignData: BaseTableRef
    {
        public int Id { get; set; }
        public int DataInforsId { get; set; }
        public string? UserAssign { get; set; }
        public int ProjectId { get; set; }

    }
}
