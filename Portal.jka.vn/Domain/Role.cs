﻿using Portal.jka.vn.Services;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.jka.vn.Domain
{
    [Table("Roles", Schema = "Identity")]
    public class Role:BaseTableRef
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
 
    }
}
