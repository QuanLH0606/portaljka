﻿using Portal.jka.vn.Services;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.jka.vn.Domain
{
    [Table("Projects", Schema = "Catalog")]
    public class Project: BaseTableRef
    {
        public int Id { get; set; }
        public string? Reports { get; set; }
        public string? Status { get; set; }
        public int? TotalRecord { get; set; }
        public int? RemainCheck { get; set; }
        public int? RemainSupervisor { get; set; } = 0;
        public bool? Training { get; set; } = false;
    }
}
