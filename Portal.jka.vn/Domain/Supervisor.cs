﻿using Portal.jka.vn.Services;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.jka.vn.Domain
{
    [Table("Supervisors", Schema = "Catalog")]
    public class Supervisor:BaseTableRef
    {
        public int Id { get; set; }
        public int DataInforId { get; set; }
        public int ProjectId { get; set; }
        public string? UserCheck { get; set; }
        public string? Url { get; set; }
    }
}
