﻿using Portal.jka.vn.Services;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.jka.vn.Domain
{
    [Table("DataInfors", Schema = "Catalog")]
    public class DataInfor: BaseTableRef
    {
        public int Id { get; set; }
        public string? LegalBusiness { get;  set; }
        public string? BillingDescriptor { get;  set; }
        public string? MerchantDBA { get;  set; }
        public string? MCC { get;  set; }
        public string? ID1 { get;  set; }
        public string? MasterCardICA { get;  set; }
        public string? VisaBIN { get;  set; }
        public string? Address1 { get;  set; }
        public string? Address2 { get;  set; }
        public string? City { get;  set; }
        public string? RegionState { get;  set; }
        public string? Country { get;  set; }
        public string? PostalZipCode { get;  set; }
        public string? PhoneNumber { get;  set; }
        public string? EmailAddress { get;  set; }
        public string? PrincipalName { get;  set; }
        public string? PrincipalAddress1 { get;  set; }
        public string? PrincipalAddress2 { get;  set; }
        public string? PrincipalCity { get;  set; }
        public string? PrincipalRegionState { get;  set; }
        public string? PrincipalCountry { get;  set; }
        public string? PrincipalPostalZipCode { get;  set; }
        public string? PrincipalPhoneNumber { get;  set; }
        public string? PrincipalEmailAddress { get;  set; }
        public string? PrimaryMerchantContactName { get;  set; }
        public string? ProductsServicesDescription { get;  set; }
        public string? DataField1 { get;  set; }
        public string? DataField2 { get;  set; }
        public string? DataField3 { get;  set; }
        public string? Analyst { get;  set; }
        public string? BusinessDescription { get;  set; }
        public string? MDM { get;  set; }
        public string? OriginalMerchantDBA { get;  set; }
        public string? ContactName { get;  set; }
        public string? OriginalProductsServicesDescription { get;  set; }
        public string? OriginalEmailAddress { get;  set; }
        public string? OriginalPrimaryMerchantContactName { get;  set; }
        public string? OriginalLegalBusinessMerchantName { get;  set; }
        public string? OriginalBillingDescriptor { get;  set; }
        public string? Address3 { get;  set; }
        public string? Fax { get;  set; }
        public string? SuggestedUrl { get;  set; }
        public string? Pre1 { get;  set; }
        public string? Pre2 { get;  set; }
        public string? Pre3 { get;  set; }
        public string? Pre4 { get;  set; }
        public string? Pre5 { get;  set; }
        public string? Pre6 { get;  set; }
        public string? Pre7 { get;  set; }
        public string? Pre8 { get;  set; }
        public string? Pre9 { get;  set; }
        public string? Pre10 { get;  set; }
        public string? Pre11 { get;  set; }
        public string? Pre12 { get;  set; }
        public string? Pre13 { get;  set; }
        public string? Pre14 { get;  set; }
        public string? Pre15 { get;  set; }
        public int ProjectId { get; set; }
    }
}
