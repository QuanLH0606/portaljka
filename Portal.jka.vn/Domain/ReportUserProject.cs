﻿using Portal.jka.vn.Services;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.jka.vn.Domain
{
    [Table("ReportUserProjects", Schema = "Catalog")]
    public class ReportUserProject: BaseTableRef
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public int NumberRecord { get; set; }
        public string UserReport { get; set; }
    }
}
