﻿using Portal.jka.vn.Services;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.jka.vn.Domain
{
    [Table("Users", Schema = "Identity")]
    public class User : BaseTableRef
    {
        public int Id { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? ImageUrl { get; set; }
        public bool? IsActive { get; set; }
        public int? RoleId { get; set; }
     
    }
}
