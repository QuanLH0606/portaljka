﻿using Portal.jka.vn.Services;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.jka.vn.Domain
{
    [Table("QADatas", Schema = "Catalog")]
    public class QAData: BaseTableRef
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int DataInforId { get; set; }
        public string? UserSend { get; set; }
        public string? AnalystNote { get; set; }
        public string? ValidUrl { get; set; }
        public string? InValidUrl { get; set; }
    }
}
