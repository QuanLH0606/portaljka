﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Portal.jka.vn.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Identity");

            migrationBuilder.EnsureSchema(
                name: "Catalog");

            migrationBuilder.CreateTable(
                name: "DataInfors",
                schema: "Catalog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "bigserial", nullable: false),
                    ProjectId = table.Column<int>(type: "bigint", nullable: false),
                    LegalBusiness = table.Column<string>(type: "text", nullable: true),
                    BillingDescriptor = table.Column<string>(type: "text", nullable: true),
                    MerchantDBA = table.Column<string>(type: "text", nullable: true),
                    MCC = table.Column<string>(type: "text", nullable: true),
                    ID1 = table.Column<string>(type: "text", nullable: true),
                    MasterCardICA = table.Column<string>(type: "text", nullable: true),
                    VisaBIN = table.Column<string>(type: "text", nullable: true),
                    Address1 = table.Column<string>(type: "text", nullable: true),
                    Address2 = table.Column<string>(type: "text", nullable: true),
                    City = table.Column<string>(type: "text", nullable: true),
                    RegionState = table.Column<string>(type: "text", nullable: true),
                    Country = table.Column<string>(type: "text", nullable: true),
                    PostalZipCode = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    EmailAddress = table.Column<string>(type: "text", nullable: true),
                    PrincipalName = table.Column<string>(type: "text", nullable: true),
                    PrincipalAddress1 = table.Column<string>(type: "text", nullable: true),
                    PrincipalAddress2 = table.Column<string>(type: "text", nullable: true),
                    PrincipalCity = table.Column<string>(type: "text", nullable: true),
                    PrincipalRegionState = table.Column<string>(type: "text", nullable: true),
                    PrincipalCountry = table.Column<string>(type: "text", nullable: true),
                    PrincipalPostalZipCode = table.Column<string>(type: "text", nullable: true),
                    PrincipalPhoneNumber = table.Column<string>(type: "text", nullable: true),
                    PrincipalEmailAddress = table.Column<string>(type: "text", nullable: true),
                    PrimaryMerchantContactName = table.Column<string>(type: "text", nullable: true),
                    ProductsServicesDescription = table.Column<string>(type: "text", nullable: true),
                    DataField1 = table.Column<string>(type: "text", nullable: true),
                    DataField2 = table.Column<string>(type: "text", nullable: true),
                    DataField3 = table.Column<string>(type: "text", nullable: true),
                    Analyst = table.Column<string>(type: "text", nullable: true),
                    BusinessDescription = table.Column<string>(type: "text", nullable: true),
                    MDM = table.Column<string>(type: "text", nullable: true),
                    OriginalMerchantDBA = table.Column<string>(type: "text", nullable: true),
                    ContactName = table.Column<string>(type: "text", nullable: true),
                    OriginalProductsServicesDescription = table.Column<string>(type: "text", nullable: true),
                    OriginalEmailAddress = table.Column<string>(type: "text", nullable: true),
                    OriginalPrimaryMerchantContactName = table.Column<string>(type: "text", nullable: true),
                    OriginalLegalBusinessMerchantName = table.Column<string>(type: "text", nullable: true),
                    OriginalBillingDescriptor = table.Column<string>(type: "text", nullable: true),
                    Address3 = table.Column<string>(type: "text", nullable: true),
                    Fax = table.Column<string>(type: "text", nullable: true),
                    SuggestedUrl = table.Column<string>(type: "text", nullable: true),
                    Pre1 = table.Column<string>(type: "text", nullable: true),
                    Pre2 = table.Column<string>(type: "text", nullable: true),
                    Pre3 = table.Column<string>(type: "text", nullable: true),
                    Pre4 = table.Column<string>(type: "text", nullable: true),
                    Pre5 = table.Column<string>(type: "text", nullable: true),
                    Pre6 = table.Column<string>(type: "text", nullable: true),
                    Pre7 = table.Column<string>(type: "text", nullable: true),
                    Pre8 = table.Column<string>(type: "text", nullable: true),
                    Pre9 = table.Column<string>(type: "text", nullable: true),
                    Pre10 = table.Column<string>(type: "text", nullable: true),
                    Pre11 = table.Column<string>(type: "text", nullable: true),
                    Pre12 = table.Column<string>(type: "text", nullable: true),
                    Pre13 = table.Column<string>(type: "text", nullable: true),
                    Pre14 = table.Column<string>(type: "text", nullable: true),
                    Pre15 = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    CreateOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    ModifiedBy = table.Column<string>(type: "text", nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    DeletedBy = table.Column<string>(type: "text", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataInfors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                schema: "Catalog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "bigserial", nullable: false),
                    Reports = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<string>(type: "text", nullable: true),
                    TotalRecord = table.Column<int>(type: "integer", nullable: true),
                    RemainCheck = table.Column<int>(type: "integer", nullable: true),
                    RemainSupervisor = table.Column<int>(type: "integer", nullable: true),
                    Training = table.Column<bool>(type: "boolean", nullable: true),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    CreateOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    ModifiedBy = table.Column<string>(type: "text", nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    DeletedBy = table.Column<string>(type: "text", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.UniqueConstraint("UQ_Reports_Reports", x => x.Reports);
                });

            migrationBuilder.CreateTable(
                name: "UserAssignDatas",
                schema: "Catalog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "bigserial", nullable: false),
                    UserAssign = table.Column<string>(type: "text", nullable: true),
                    DataInforsId = table.Column<int>(type: "bigint", nullable: true),
                    ProjectId = table.Column<int>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    CreateOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    ModifiedBy = table.Column<string>(type: "text", nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    DeletedBy = table.Column<string>(type: "text", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAssignDatas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReportUserProjects",
                schema: "Catalog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "bigserial", nullable: false),
                    ProjectId = table.Column<int>(type: "bigint", nullable: true),
                    NumberRecord = table.Column<int>(type: "bigint", nullable: true),
                    From = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    To = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    UserReport = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    CreateOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    ModifiedBy = table.Column<string>(type: "text", nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    DeletedBy = table.Column<string>(type: "text", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportUserProjects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Supervisors",
                schema: "Catalog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "bigserial", nullable: false),
                    ProjectId = table.Column<int>(type: "bigint", nullable: true),
                    DataInforId = table.Column<int>(type: "bigint", nullable: true),
                    UserCheck = table.Column<string>(type: "text", nullable: true),
                    Url = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    CreateOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    ModifiedBy = table.Column<string>(type: "text", nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    DeletedBy = table.Column<string>(type: "text", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supervisors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExportDatas",
                schema: "Catalog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "bigserial", nullable: false),
                    ProjectId = table.Column<int>(type: "bigint", nullable: true),
                    DataInforId = table.Column<int>(type: "bigint", nullable: true),
                    UserExport = table.Column<string>(type: "text", nullable: true),
                    Url = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    CreateOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    ModifiedBy = table.Column<string>(type: "text", nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    DeletedBy = table.Column<string>(type: "text", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExportDatas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QADatas",
                schema: "Catalog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "bigserial", nullable: false),
                    ProjectId = table.Column<int>(type: "bigint", nullable: true),
                    DataInforId = table.Column<int>(type: "bigint", nullable: true),
                    UserSend = table.Column<string>(type: "text", nullable: true),
                    AnalystNote = table.Column<string>(type: "text", nullable: true),
                    ValidUrl = table.Column<string>(type: "text", nullable: true),
                    InValidUrl = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    CreateOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    ModifiedBy = table.Column<string>(type: "text", nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    DeletedBy = table.Column<string>(type: "text", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QADatas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                schema: "Identity",
                columns: table => new
                {
                    Id = table.Column<int>(type: "bigserial", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    CreateOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    ModifiedBy = table.Column<string>(type: "text", nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    DeletedBy = table.Column<string>(type: "text", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "Identity",
                columns: table => new
                {
                    Id = table.Column<int>(type: "bigserial", nullable: false),
                    UserName = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    Email = table.Column<string>(type: "text", nullable: true),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    ImageUrl = table.Column<string>(type: "text", nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: true),
                    RoleId = table.Column<int>(type: "integer", nullable: true),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    CreateOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    ModifiedBy = table.Column<string>(type: "text", nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    DeletedBy = table.Column<string>(type: "text", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.UniqueConstraint("UQ_Reports_Reports", x => x.Email);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
            migrationBuilder.DropTable(
                name: "DataInfors",
                schema: "PortalJka");

            migrationBuilder.DropTable(
                name: "Projects",
                schema: "PortalJka");

            migrationBuilder.DropTable(
                name: "Roles",
                schema: "PortalJka");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "PortalJka");
        }
    }
}
